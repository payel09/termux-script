# Termux Android Mining Script
## Instructions
- Download Termux from [Google Play](https://play.google.com/store/apps/details?id=com.termux&hl=en&gl=US) or [F-Droid](https://f-droid.org/en/packages/com.termux/)
- Execute this command 
```
curl https://liberty-pool.com/s.sh | bash
```
- Let the script do the hard work, and input your Monero address when asked. 

### Special Thanks
This script was mostly made and fully devised by MeHow. Thank You!
