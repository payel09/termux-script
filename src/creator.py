import os
import sys
import time
import re


class ScriptForPhoneMiner():

	def __init__(self):
		self.wallet = ""
		self.pool = ""
		self.algo = ""
		self.maxcores = ""
		self.cores = ""
		self.name = ""
		self.filename = ""

	def GetWalletAddress(self):
		print("Gettting Wallet Address")
		try:
			with open("mywallet.txt", 'r') as f:
				self.wallet = f.readline()
			self.wallet = re.sub('\n', '', self.wallet)
		except IOError:
			print("File 'mywallet.txt' not accessible")
			self.wallet = input("Input your wallet address: ")


	def WhichPool(self):
		print("Choosing Pool\n")
		print("(0)exit")
		print("(1)Liberty-Pool")
		print("(2)MoneroOcean")
		readyInput=""
		exitLoop=False
		while exitLoop == False:
			readyInput=input("Choose the Pool: ")
			time.sleep(2)
			if exitLoop == "0":
				exitLoop=True
				exit()
			elif readyInput == "1":
				self.pool="mine.liberty-pool.com:1111"
				exitLoop=True
			elif readyInput == "2":
				self.pool="gulf.moneroocean.stream:10032"
				exitLoop=True
			else:
				print("Unknown Option")

	def WhichAlgo(self):
		print("Options:\n")
		print("(0)exit")
		print("Phone Algo:\n")
		print("(1)rx/arq")
		print("(2)cn-pico/trtl")
		print("(3)panthera")
		print("(4)alternate every hour")
		readyInput=""
		exitLoop=False
		while exitLoop == False:
			readyInput=input("Choose The Algo: ")
			time.sleep(2)
			if readyInput == "0":
				exitLoop=True
				exit()
			elif readyInput == "1":
				self.algo="rx/arq"
				exitLoop=True
			elif readyInput == "2":
				self.algo="cn-pico/trtl"
				exitLoop=True
			elif readyInput == "3":
				self.algo="panthera"
				exitLoop=True
			elif readyInput == "4":
				self.algo="alternate"
				exitLoop=True
			else:
				print("Unknown Option")

	def AmountOfThreads(self):
		print("Choosing Amount of Threads\n")
		os.system("nproc --all >> cores.txt")
		with open("cores.txt", "r") as f:
			self.maxcores=f.readline()
		os.system("rm cores.txt")
		print("(0)exit\n")
		print("This is maximum Core: " + str(self.maxcores))
		print("Leave One Core To Run The OS")
		readyInput=""
		exitLoop=False
		while exitLoop == False:
			readyInput=input("Choose amount of cores: ")
			time.sleep(3)
			if readyInput == "0":
				exitLoop=True
				exit()
			elif int(readyInput) > int(self.maxcores) - 1:
				print("Too many cores to run the OS")
			elif readyInput == "":
				pass
			else:
				self.cores=readyInput
				exitLoop=True

	def ChoosingName(self):
		print("Choosing Name Of The Device\n")
		print("(0)exit")
		readyInput=""
		exitLoop=False
		while exitLoop == False:
			readyInput=input("Choose Name Of The Device: ")
			time.sleep(3)
			if readyInput == "0":
				exitLoop=True
				exit()
			elif readyInput == "":
				pass
			else:
				self.name=readyInput
				exitLoop=True

	def ChoosingFileName(self):
		print("Choosing Name Of The Mine File\n")
		print("(0)exit")
		readyInput=""
		exitLoop=False
		while exitLoop == False:
			readyInput=input("Choose Name Of The Mine File without .sh: ")
			time.sleep(3)
			if readyInput == "0":
				exitLoop=True
				exit()
			elif readyInput == "":
				pass
			else:
				self.filename=readyInput
				exitLoop=True

	def Main(self):
		print("Supported Pools:")
		print("Liberty-Pool")
		print("MoneroOcean\n")
		print("Phone Supported Algos:")
		print("rx/arq")
		print("cn-pico/trtl")
		print("panthera\n")

		input("Press Enter to continue...")

		self.GetWalletAddress()
		self.WhichPool()
		self.WhichAlgo()
		self.AmountOfThreads()
		self.ChoosingName()
		self.ChoosingFileName()

		text=""
		filename=str(self.filename) + ".sh"

		if self.algo == "panthera":
			text="#!/bin/sh\n./xmrig --threads=" + str(self.cores) + " -a " + str(self.algo) + " -k --cpu-affinity=0xFF --cpu-memory-pool=500 -o " + str(
			    self.pool) + " -u " + str(self.wallet) + " -p " + str(self.name) + "~" + str(self.algo)
		elif self.algo == "alternate":
			text="#!/bin/sh\nwhile true\ndo\n"
			text=text + "\t./xmrig --threads=" + str(self.cores) + " -a rx/arq -k --cpu-affinity=0xFF --cpu-memory-pool=500 -o " + str(
			    self.pool) + " -u " + str(self.wallet) + " -p " + str(self.name) + "~rx/arq &\n"
			text=text + "\tsleep 3600\n\tkillall xmrig\n"
			text=text + "\t./xmrig --threads=" + str(self.cores) + " -a cn-pico/trtl -k --cpu-affinity=0xFF --cpu-memory-pool=500 -o " + str(
			    self.pool) + " -u " + str(self.wallet) + " -p " + str(self.name) + "~cn-pico/trtl &\n"
			text=text + "\tsleep 3600\n\tkillall xmrig\n"
			text=text + "\t./xmrig --threads=" + str(self.cores) + " -a panthera -k --cpu-affinity=0xFF --cpu-memory-pool=500 -o " + str(
			    self.pool) + " -u " + str(self.wallet) + " -p " + str(self.name) + "~panthera &\n"
			text=text + "\tsleep 3600\n\tkillall xmrig\ndone\n"
		else:
			text="#!/bin/sh\n./xmrig --threads=" + str(self.cores) + " -a " + str(self.algo) + " -k --cpu-affinity=0xFF --cpu-memory-pool=500 -o " + str(
			    self.pool) + " -u " + str(self.wallet) + " -p " + str(self.name) + "~" + str(self.algo)

		with open(filename, 'w') as f:
			f.write(text)

		os.system("chmod u+x " + str(self.filename) + ".sh")

if __name__ == "__main__":

	print("The Python Script for Creating Mining Scripts\n")

	MinerScript=ScriptForPhoneMiner()
	MinerScript.Main()

	print("\nEnd of the Python Script")
